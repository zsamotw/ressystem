# RESERVATION SYSTEM

## Actors
* Individual user
* Institution user
* Admin

## Model
* User : Individual, Institution, Admin
* Educator
* Event : ForInstitution, ForIndividual
* Recipient (age category)
* Status: Active, Realized, Canceled

## Current functionality
* Admin could add event -> it should set kind of event (for individuals or for institutions), date, descritption, teacher, age category, maximum of participants.
* Admin could add teacher do database.
* Admin could add event.
* Admin could delete event (delete from database) or cancel event(change it's status)
* Admin could generate report.
* User(Individual and Institution) could create account and login.
* User(Individual and Institution) could list events and subscribe them. In case of Institution, user should add some more information as size of group, avarage age etc.
* User(Individual and Institution) could list subscribed  events.
* User(Individual and Institution) could update it's data.
* There are some filters for  events(by date, type, keyword, educator, recipient category, status)
