package test.models

import java.time.LocalDateTime
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import org.scalatest._
import org.scalatest.Matchers._
import play.api.test._
import play.api.test.Helpers._
import models._
import models.events.{EventForIndividual, EventForInstitution, Events, EventFilters}
import models.reports._
import models.Users._

/*
 *Class ReportTest works with mock database implemented in DB.scala
 */


class ReportTest extends PlaySpec with BeforeAndAfter {

  before {
    DB.events = Set(
      EventForIndividual(1, LocalDateTime.of(2019, 7, 27, 10, 10), LocalDateTime.of(2019, 11, 30, 10, 10), "wydazenie", "jakies wydarzenie", Educator("anna jakas", "email", 1234), 150, Recipients.recipients(0), "rooom 1", "torch is required",  10, false, Set()),
      EventForInstitution(2, LocalDateTime.now().minusDays(2), LocalDateTime.now().minusDays(2), "dla instytyucji", "cos tam", Educator("anna jakas", "emal", 1234), 200, Recipients.recipients(0), "romm 3", "Keep smile", 10, false, None, None,None, None)
    )
  }

  "Raport " should {
    "be printed" in {
      val report = new SimpleReport(DB.events)
      println(report)
    }
  }

  "Raport of countEvents " should {
    "be 2" in {
      val report = new SimpleReport(DB.events)
      report.countEvents mustBe 2
    }
  }
}
