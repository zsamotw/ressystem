

// import org.scalatestplus.play._
// import org.scalatestplus.play.guice._
// import play.api.test._
// import play.api.test.Helpers._
// import models._
// import models.Functionalities._

// class LibraryTest extends PlaySpec {
//   "User " should {
//     "be added to users" in {
//       val user = User("Marek", "email")
//       add(user)
//       DB.users must contain (user)
//     }
//   }
//   "Event " should {
//     "be added to events" in {
//       val event = Event(Date(), "event", "email", "anaa", null, null)
//       add(event)
//       DB.events must contain (event)
//     }
//   }
//   "Institution " should {
//     "be added to institutions" in {
//       val institution = Institution("Museum", "contact")
//       add(institution)
//       DB.institutions must contain (institution)
//     }
//   }

//   "Event" should {
//     "be subscribed" in {
//       DB.events = List()
//       val user = User("Marek", "email")
//       val event = Event(Date(), "event", "email", "anaa", null, null)
//       val event2 = Event(Date(), "event", "email", "anaa", null, user)
//       add(event)
//       DB.events = subscribe(user, event)
//       val resEvent = DB.events.filter(_ == event2).head
//       resEvent.subscriber must equal (user)
//     }
//   }
  
//   "Filter by user" should {
//     "return one elem list" in {
//       DB.events = List()
//       val user = User("Ewa", "mail")
//       val event = Event(Date(), "event", "email", "anaa", null, null)
//       val event2 = Event(Date(), "event", "email", "anaa", null, user)
//       add(event)
//       add(event2)
//       val result = filter(DB.events, user)
//       result must equal (Some(List(event2)))
//     }
//   }
// }
