package test.models

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import org.scalatest._
import org.scalatest.Matchers._
import play.api.test._
import play.api.test.Helpers._
import models._
import models.Users._

/*
 *Class UserTest works with mock database implemented in DB.scala
 */


class UserTest extends PlaySpec with BeforeAndAfter {

  before {
    DB.users = Set(Individual("t", "t", "tomasz", 1234))
  }

  "User " should {
    "be added to users" in {
      val user = Individual("email", "password", "some user", 12345)
      Users.save(user)
      DB.users must contain (user)
    }
  }

  "User with email: \"email\" " should {
    "be added to users and then find by email" in {
      val user = Individual("email", "password", "some user", 12345)
      Users.save(user)
      val thisUser = Users.find("email")
      thisUser must be (Right(Individual("email", "password", "some user", 12345)))
    }
  }

  "User with email: \"wrongemail\" " should {
    "be imposible to find and return value Left" in {
      val thisUser = Users.find("wrongemail")
      thisUser must be (Left("There is no such user in database"))
    }
  }
}
