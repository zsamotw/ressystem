package test.models

import java.time.LocalDateTime
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import org.scalatest._
import org.scalatest.Matchers._
import play.api.test._
import play.api.test.Helpers._
import models._
import models.events.{EventForIndividual, EventForInstitution, Events, EventFilters}
import models.Users._

/*
 *Class EventTest works with mock database implemented in DB.scala
 */


class EventTest extends PlaySpec with BeforeAndAfter {

  before {
    DB.events = Set(
      EventForIndividual(1, LocalDateTime.of(2019, 7, 27, 10, 10), LocalDateTime.of(2019, 11, 30, 10, 10), "wydazenie", "jakies wydarzenie", Educator("anna jakas", "email", 1234), 150, Recipients.recipients(0), "rooom 1", "torch is required",  10, false, Set()),
      EventForInstitution(2, LocalDateTime.now().minusDays(2), LocalDateTime.now().minusDays(2), "dla instytyucji", "cos tam", Educator("anna jakas", "emal", 1234), 200, Recipients.recipients(0), "romm 3", "Keep smile", 10, false, None, None,None, None)
    )
  }

  "Event " should {
    "be added to events" in {
      val event = EventForIndividual(2, LocalDateTime.now(), LocalDateTime.now(),"event", "jakis event", Educator("anna jakas", "email", 1234), 200, Recipients.recipients(0), "Room", "Come here",  10, false, Set())
      Events.save(event)
      DB.events must contain (event)
    }
  }

  "Event with id == 3 " should {
    "be added to users and then find by id" in {
      val id = 3
      val event = EventForIndividual(id, LocalDateTime.of(2000, 11, 30, 10, 10), LocalDateTime.of(2000, 11, 30, 10, 10), "event3", "jakis event", Educator("anna jakas", "email", 1234), 150,  Recipients.recipients(0), "room", "info",  10, false, Set())
      Events.save(event)
      val thisEvent = Events.find(id)
      thisEvent must be (Right(EventForIndividual(id, LocalDateTime.of(2000, 11, 30, 10, 10), LocalDateTime.of(2000, 11, 30, 10, 10), "event3", "jakis event", Educator("anna jakas", "email", 1234), 150,  Recipients.recipients(0), "room", "info" , 10, false, Set())))
    }
  }

  "Event with id == 0 " should {
    "be imposible to find by id and find method should return Left value" in {
      val id = 0
      val thisEvent = Events.find(id)
      thisEvent must be (Left("There is no such event in datebase"))
    }
  }

  "Events with two event" should {
    "return list of all events as set with lenght 2" in {
      val all = Events.listAll
      all.size must be (2)
    }
  }

  "Events" should {
    "update event" in {
      val toUpadate = (EventForIndividual(1, LocalDateTime.of(2000, 10, 6, 12, 30), LocalDateTime.of(2000, 10, 6, 12, 30), "updated", "jakies wydarzenie", Educator("anna jakas", "email", 1234) , 200, Recipients.recipients(0), "romm", "info" , 10, false, Set()))
      Events.update(toUpadate)
      DB.events must contain (toUpadate)
    }
  }

  "Events" should {
    "filter events by date -> return events after the date" in {
      val filtered = Events.filter(EventFilters.fromDateFilter(Some(LocalDateTime.now())))
      filtered.size must be (1) 

    }
  }
}
