
package controllers

import javax.inject._
import models.Users._
import models._
import models.forms.LoginForm._
import models.forms.UserForm._
import play.api.mvc._

@Singleton
class SessionController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with play.api.i18n.I18nSupport {

  lazy val conn = "connected"
  lazy val authMessage = "Login or create user account!"
  lazy val incorrectDataMessage = "Fill form correctly"

  def index(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index(authMessage, userForm, loginForm))
  }

  def login(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    loginForm.bindFromRequest().fold(
      formWithError => {
        BadRequest(views.html.index(incorrectDataMessage, userForm, formWithError))
      },
      loginData => {
        Logins.validate(loginData) match {
          case Some(email) =>
            Users.find(email) match {
              case Right(user) =>
                user match {
                  case individual: Individual =>
                    val message = s"You are in panel. Hallo  ${individual.name}"
                    Ok(views.html.userPanel(user, message)).withSession(conn -> email)
                  case institution: Institution =>
                    val message = s"You are in panel. Hallo  ${institution.repName} from ${institution.name}"
                    Ok(views.html.userPanel(user, message)).withSession(conn -> email)
                  case admin: Admin =>
                    val message = s"You are in panel dear Admin. Hallo  ${admin.name}"
                    Ok(views.html.adminPanel(message)).withSession(conn -> email)
                }
              case Left(message) =>
                Ok(views.html.index(message, userForm, loginForm))
            }
          case None =>
            val message = "You can't login with this email and password"
            Ok(views.html.index(message, userForm, loginForm))
        }
      })
  }

  def logout(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val message = "Bye Bye"
    Ok(views.html.index(message, userForm, loginForm)).withNewSession
  }
}
