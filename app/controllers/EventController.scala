
package controllers

import javax.inject._
import play.api.mvc._
import models._
import models.events.{EventFilters, EventForIndividual, EventForInstitution, Events}
import models.forms.AdditionalDataFromUserForm._
import models.forms.IdForm._
import models.forms.LoginForm._
import models.forms.UserForm._
import models.forms.EventFromAdminForm._
import models.forms.DataToFilterEventsByAdminForm._
import models.forms.DataToFilterEventsByUserForm._
import models.forms.DataToGenerateReport
import models.forms.ReportGenerateForm._
import models.reports.SimpleReport
import models.Helpers.eventCopy
import models.Users._


@Singleton
class EventController @Inject() (cc: ControllerComponents) extends AbstractController(cc) with play.api.i18n.I18nSupport {

  lazy val conn = "connected"
  lazy val authMessage = "Login or create user account!"
  lazy val incorrectFormMessage = "Fill form correctly"


  def delete(eventId: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val message = "Not implemented"
    Ok(views.html.index(message, userForm, loginForm))
  }

  def cancel(eventId: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val message = "Not implemented"
    Ok(views.html.index(message, userForm, loginForm))
  }

  def subscribe(eventId: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      val eventAsEither = DB.getEvent(eventId.toInt)
      val userAsEither = DB.getUser(userEmail)
      (eventAsEither, userAsEither) match {
        case (Right(event), Right(user)) =>
          (event, user) match {
            case (e: EventForIndividual, u: Individual) =>
              val newEvent = e.copy(subscribers = e.subscribers + u)
              Events.update(newEvent)
              val message = s"New event subscribed: ${e.title}"
              val(events, educators, recipients) = Helpers.getDataForView(user)
              Ok(views.html.allEvents(
                events,
                user,
                educators,
                recipients,
                Events.kinds,
                Statuses.statuses,
                message,
                idForm,
                dataToFilterEventsByUserForm,
                dataToFilterEventsByAdminForm))
            case (e: EventForInstitution, u: Institution) =>
              Ok(views.html.subscribeEvent(e, u, additionalDataFromUserForm))
            case (_, _) =>
              val message = "Ooops something wrong! It is not event for you!"
              Ok(views.html.index(message, userForm, loginForm)).withNewSession
          }
        case (Left(messageEvent), Left(messageUser)) =>
          val message = s"$messageEvent and $messageUser"
          Ok(views.html.index(message, userForm, loginForm)).withNewSession
        case (Left(messageEvent), _) =>
          Ok(views.html.index(messageEvent, userForm, loginForm)).withNewSession
        case (_, Left(messageUser)) =>
          Ok(views.html.index(messageUser, userForm, loginForm)).withNewSession
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def unsubscribe(eventId: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      val eventAsEither = DB.getEvent(eventId.toInt)
      val userAsEither = DB.getUser(userEmail)
      (eventAsEither, userAsEither) match {
        case (Right(event), Right(user)) =>
          (event, user) match {
            case (e: EventForIndividual, u: Individual) =>
              val newEvent = e.copy(subscribers = e.subscribers - u)
              Events.update(newEvent)
              Redirect(routes.EventController.listSubscribedEvents(user.email))
            case (e: EventForInstitution, u: Institution) =>
              val newEvent = e.copy(subscriber = None)
              Events.update(newEvent)
              Redirect(routes.EventController.listSubscribedEvents(user.email))
            case (_, _) =>
              val message = "Oooops. Something wrong. It is not event subscribed by you!"
              Ok(views.html.index(message, userForm, loginForm)).withNewSession
          }
        case (Left(messageEvent), Left(messageUser)) =>
          val message = s"$messageEvent and $messageUser"
          Ok(views.html.index(message, userForm, loginForm)).withNewSession
        case (Left(messageEvent), _) =>
          Ok(views.html.index(messageEvent, userForm, loginForm)).withNewSession
        case (_, Left(messageUser)) =>
          Ok(views.html.index(messageUser, userForm, loginForm)).withNewSession
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def addDataForSubscribe(eventId: Int): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      Users.find(userEmail) match {
        case Right(user) =>
          Events.find(eventId) match {
            case Right(event) =>
              additionalDataFromUserForm.bindFromRequest().fold(
                formWithError => {
                  BadRequest(views.html.subscribeEvent(event, user, formWithError))
                },
                eventData => {
                  val forIndividualMessage = "Ooops. It is only for institutions. Not for individuals."
                  user match {
                    case institution: Institution =>
                      event match {
                        case e: EventForIndividual =>
                          Ok(views.html.index(forIndividualMessage, userForm, loginForm)).withNewSession
                        case e: EventForInstitution =>
                          val newEvent: EventForInstitution = eventCopy(eventData, institution, e)
                          Events.update(newEvent)
                          val message = s"New event subscribed: ${e.title}"
                          val(events, educators, recipients) = Helpers.getDataForView(user)
                          Ok(views.html.allEvents(
                               events,
                               user,
                               educators,
                               recipients,
                               Events.kinds,
                               Statuses.statuses,
                               message,
                               idForm,
                               dataToFilterEventsByUserForm,
                               dataToFilterEventsByAdminForm))
                      }
                    case individual: Individual =>
                      Ok(views.html.index(forIndividualMessage, userForm, loginForm)).withNewSession
                  }
                })
            case Left(message) =>
              Ok(views.html.index(message, userForm, loginForm)).withNewSession
          }
        case Left(message) =>
          Ok(views.html.index(message, userForm, loginForm)).withNewSession
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def newEvent(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      val educators = Educators.listAll
      val message = "Fill form to add event."
    Ok(views.html.newEvent(educators, Recipients.recipients, message, eventAdminForm))
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def addEvent(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      eventAdminForm.bindFromRequest().fold(
        formWithError => {
          val educators = Educators.listAll
          BadRequest(views.html.newEvent(educators, Recipients.recipients, incorrectFormMessage, formWithError))
      },
        eventData => {
          Events.form2Event(eventData) match {
            case Right(event) =>
              Events.save(event)
              val message = s"New event had been added: ${event.title}"
              Ok(views.html.adminPanel(message))
            case Left(message) =>
              val educators = Educators.listAll
              Ok(views.html.newEvent(educators, Recipients.recipients, message, eventAdminForm))
          }
        })
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def filter(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      Users.find(userEmail) match {
        case Right(user) =>
          user match {
            case individual: Individual =>
              dataToFilterEventsByUserForm.bindFromRequest.fold(
                formWithError => {
                  val(events, educators, recipients) = Helpers.getDataForView(user)
                  Ok(views.html.allEvents(
                       events,
                       user,
                       educators,
                       recipients,
                       Events.kinds,
                       Statuses.statuses,
                       incorrectFormMessage,
                       idForm,
                       formWithError,
                       dataToFilterEventsByAdminForm))
                },
                dataToFilter => {
                  val(events,  educators, recipients, message) = Events.getDataWithFilteredEvents(dataToFilter, user)
                  Ok(views.html.allEvents(
                       events,
                       user,
                       educators,
                       recipients,
                       Events.kinds,
                       Statuses.statuses,
                       message,
                       idForm,
                       dataToFilterEventsByUserForm,
                       dataToFilterEventsByAdminForm))
                }
              )
            case institution: Institution =>
              dataToFilterEventsByUserForm.bindFromRequest.fold(
                formWithError => {
                  val(events, educators, recipients) = Helpers.getDataForView(user)
                  Ok(views.html.allEvents(
                       events,
                       user,
                       educators,
                       recipients,
                       Events.kinds,
                       Statuses.statuses,
                       incorrectFormMessage,
                       idForm,
                       formWithError,
                       dataToFilterEventsByAdminForm))
                },
                dataToFilter => {
                  val(events,  educators, recipients, message) = Events.getDataWithFilteredEvents(dataToFilter, user)
                  Ok(views.html.allEvents(
                       events,
                       user,
                       educators,
                       recipients,
                       Events.kinds,
                       Statuses.statuses,
                       incorrectFormMessage,
                       idForm,
                       dataToFilterEventsByUserForm,
                       dataToFilterEventsByAdminForm))
                }
              )
            case admin: Admin =>
              dataToFilterEventsByAdminForm.bindFromRequest.fold(
                formWithError => {
                  val(events, educators, recipients) = Helpers.getDataForView(user)
                  Ok(views.html.allEvents(
                       events,
                       user,
                       educators,
                       recipients,
                       Events.kinds,
                       Statuses.statuses,
                       incorrectFormMessage,
                       idForm,
                       dataToFilterEventsByUserForm,
                       formWithError))

                },
                dataToFilter => {
                  val(events, educators, recipients, message) = Events.getDataWithFilteredEvents(dataToFilter, user)
                  Ok(views.html.allEvents(
                       events,
                       user,
                       educators,
                       recipients,
                       Events.kinds,
                       Statuses.statuses,
                       incorrectFormMessage,
                       idForm,
                       dataToFilterEventsByUserForm,
                       dataToFilterEventsByAdminForm))
                }
              )
          }
        case Left(message) =>
          Ok(views.html.index(message, userForm, loginForm)).withNewSession
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def listEvents(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      Users.find(userEmail) match {
        case Right(user) =>
          val message = "Events for You all there are here:"
          val(events, educators, recipients) = Helpers.getDataForView(user)
          Ok(views.html.allEvents(
               events,
               user,
               educators,
               recipients,
               Events.kinds,
               Statuses.statuses,
               message,
               idForm,
               dataToFilterEventsByUserForm,
               dataToFilterEventsByAdminForm))
        case Left(message) =>
          Ok(views.html.index(message, userForm, loginForm)).withNewSession
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def listSubscribedEvents(userEmail: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      Users.find(userEmail) match {
        case Right(user) =>
          val message = "Your Events here"
          val events = Events.listSubscribedEvents(user)
          Ok(views.html.subscribedEvents(events, user, message, idForm))
        case Left(message) =>
          Ok(views.html.index(message, userForm, loginForm)).withNewSession
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def showReportPanel(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail => Users.find(userEmail) match {
        case Right(user) =>
          user match {
            case u: Admin =>
              val message = "Create you own report"
              Ok(views.html.reportPanel(message, dataToGenerateReport))
            case  _ =>
              val message = "You can create raport. It is possible only for admin"
              Ok(views.html.index(message, userForm, loginForm)).withNewSession
          }
        case Left(message) =>
          Ok(views.html.index(message, userForm, loginForm)).withNewSession
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def generateReport(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      Users.find(userEmail) match {
        case Right(user) =>
          user match {
            case u: Admin =>
              dataToGenerateReport.bindFromRequest().fold(
                formWithError => {
                  BadRequest(views.html.reportPanel(incorrectFormMessage, formWithError))
                },
                dates => {
                  val DataToGenerateReport(from, to) = dates
                  val toLocalDate = Events.dateOpt2LocalDateOpt(to)
                  val fromLocalDate = Events.dateOpt2LocalDateOpt(from)
                  val events = Events.filter(
                    EventFilters.everyFilters(
                      EventFilters.toDateFilter(toLocalDate),
                      EventFilters.fromDateFilter(fromLocalDate)
                    ))
                  val report = SimpleReport(events)
                  val message = s"Raport from $from to $to."
                  Ok(views.html.displayReport(message, report))
                })
            case  _ =>
              val message = "You can create report. It is possible only for admin"
              Ok(views.html.index(message, userForm, loginForm)).withNewSession
          }
        case Left(message) =>
          Ok(views.html.index(message, userForm, loginForm)).withNewSession
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }
}
