
package controllers

import javax.inject._
import models._
import models.forms.EducatorForm._
import models.forms.LoginForm._
import models.forms.UserForm._
import play.api.mvc._

@Singleton
class EducatorController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with play.api.i18n.I18nSupport {

  lazy val conn = "connected"
  lazy val authMessage = "Login or create user account!"

  def newEducator(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      val message = s"Add new educator"
      Ok(views.html.newEducator(message, educatorForm))
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def addEducator(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      educatorForm.bindFromRequest().fold(
        formWithError => {
          val message = "Fill form correctly"
          BadRequest(views.html.newEducator(message, formWithError))
        },
        educator => {
          Educators.save(educator)
          val message = s"You have added new educator: ${educator.name}"
          Ok(views.html.newEducator(message, educatorForm))
        })
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }
}
