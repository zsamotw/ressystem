
package controllers

import javax.inject._
import models.Helpers.{copyUser, getFilledForm}
import models.Users._
import models._
import models.forms.IndividualUpdateForm._
import models.forms.InstitutionUpdateForm._
import models.forms.LoginForm._
import models.forms.UserForm._
import models.forms.{IndividualUpdateForm, InstitutionUpdateForm}
import play.api.data.Form
import play.api.mvc._

@Singleton
class UserController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with play.api.i18n.I18nSupport {

  lazy val conn = "connected"
  lazy val authMessage = "Login or create user account!"
  lazy val incorrectDataMessage = "Fill form correctly"

  def addUser(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    userForm.bindFromRequest().fold(
      formWithError => {
        BadRequest(views.html.index(incorrectDataMessage, formWithError, loginForm))
      },
      userData => {
        (userData.institutionName, userData.institutionAddress, userData.institutionKind) match {
          case (None, None, None) =>
            val individual = Individual(
              userData.email,
              userData.password,
              userData.name,
              userData.phone)

            Users.save(individual)
            val message = s"Welcome ${individual.name}"
            Ok(views.html.userPanel(individual, message)) withSession (conn -> individual.email)
          case (Some(name), Some(address), Some(instKind)) =>
            val institution = Institution(
              userData.email,
              userData.password,
              userData.name,
              userData.phone,
              name,
              address,
              instKind)

            Users.save(institution)
            val message = s"Welcome ${institution.name}"
            Ok(views.html.userPanel(institution, message)) withSession (conn -> institution.email)
          case (_, _, _) =>
            Ok(views.html.index(incorrectDataMessage, userForm, loginForm))
        }
      })
  }

  def userPanel(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      Users.find(userEmail) match {
        case Right(user) =>
          val message = s"You are in panel. Hallo  ${user.name}"
          user match {
            case individual: Individual =>
              Ok(views.html.userPanel(individual, message))
            case institution: Institution =>
              Ok(views.html.userPanel(institution, message))
            case _: Admin =>
              Ok(views.html.adminPanel(message))
          }
        case Left(_) =>
          Ok(views.html.index(authMessage, userForm, loginForm))
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def userAccount(userEmail: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      Users.find(userEmail) match {
        case Right(user) =>
          val message = s"You are in your account."
          user match {
            case individual: Individual =>
              val filledForm: Form[IndividualUpdateForm] = getFilledForm(individual)
              Ok(views.html.userAccount(individual, message, filledForm, institutionUpdateForm))
            case institution: Institution =>
              val filledForm: Form[InstitutionUpdateForm] = getFilledForm(institution)
              Ok(views.html.userAccount(institution, message, individualUpdateForm, filledForm))
            case _: Admin =>
              val message = "This option is not for you!!"
              Ok(views.html.index(message, userForm, loginForm))
          }
        case Left(_) =>
          Ok(views.html.index(authMessage, userForm, loginForm))
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }

  def updateUser(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.session.get(conn).map { userEmail =>
      val message = "Your data has been updated"
      Users.find(userEmail) match {
        case Right(user) =>
          user match {
            case individual: Individual =>
              individualUpdateForm.bindFromRequest().fold(
                formWithError => {
                  BadRequest(views.html.userAccount(individual, incorrectDataMessage, formWithError, institutionUpdateForm))
                },
                updateData => {
                  val newUser: Individual = copyUser(individual, updateData)
                  Users.update(newUser)
                  val filledForm: Form[IndividualUpdateForm] = getFilledForm(newUser)
                  Ok(views.html.userAccount(newUser, message, filledForm, institutionUpdateForm))
                }
              )
            case institution: Institution =>
              institutionUpdateForm.bindFromRequest().fold(
                formWithError => {
                  BadRequest(views.html.userAccount(institution, incorrectDataMessage, individualUpdateForm, formWithError))
                },
                updateData => {
                  val newUser: Institution = copyUser(institution, updateData)
                  Users.update(newUser)
                  val filledForm: Form[InstitutionUpdateForm] = getFilledForm(newUser)
                  Ok(views.html.userAccount(newUser, message, individualUpdateForm, filledForm))
                }
              )
            case _: Admin =>
              val message = "This option is not for you!!"
              Ok(views.html.index(message, userForm, loginForm))
          }
        case Left(_) =>
          Ok(views.html.index(authMessage, userForm, loginForm))
      }
    }.getOrElse {
      Ok(views.html.index(authMessage, userForm, loginForm))
    }
  }
}
