package models.generators

import models._

object DataToGenerate {
  val titles = List(
    "warsztat 1",
    "warsztat 2",
    "warsztat 3",
    "warsztat 4",
    "warsztat 5",
    "warsztat 6",
    "warsztat 7",
    "warsztat 8",
    "warsztat 9",
    "warsztat 10",
    "warsztat 11",
    "warsztat 12",
    "warsztat 13",
    "warsztat 14"
  )

  val descriptions = List(
    "lorem ipsum lorem ipsum",
    "ipsum lorem",
    "ipsum lorem lorem ipsum",
    "ipsum",
    "lorem",
    "lorem lorem lorem lorem",
    "ipsum ipsum ipsum"
  )

  val educators = List(
    Educator("Tomasz Pan", "tomasz@email", 1234),
    Educator("Anna Pani", "anna@email", 1234),
    Educator("Antonina Pani", "antonina@email", 1234),
    Educator("Roza Pani", "roza@email", 1234),
  )

  val salaries = List[BigDecimal](100, 150, 200, 300)

  val rooms = List(
    "Room 1",
    "Room 2",
    "Room 3",
    "Room 4"
  )

  val infos = List(
    "Important info",
    "Not important info",
    "NO info"
  )
}
