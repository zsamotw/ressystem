
package models.generators

import scala.util.Random

case class IntFromToGenerator(from: Int, to: Int) extends Generator[Int] {
  def generate: Int = from + Random.nextInt(to - from + 1)

}
