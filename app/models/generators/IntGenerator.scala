
package models.generators

object IntGenerator extends Generator[Int]{
  def generate: Int = scala.math.abs(scala.util.Random.nextInt)
}
