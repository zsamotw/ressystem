
package models.generators

import java.time._

import models.events._

trait Generator[T] {
  self =>
  def generate: T

  def map[S](f: T => S): Generator[S] = new Generator[S] {
    def generate = f(self.generate)
  }

  def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
    def generate: S = f(self.generate).generate
  }
}

object Generators {
  val _0 = 0
  val _1 = 1
  val _12 = 12
  val _20 = 20
  val _23 = 23
  val _27 = 27
  val _59 = 59
  val _60 = 60
  val _2018 = 2018

  val ofInts: Generator[Int] = for {
    i <- IntGenerator
  } yield i

  val ofBooleans: Generator[Boolean] = for {
    i <- IntGenerator
  } yield if(i % 2 == 0) true else false

  def ofDates(fromYear: Int, toYear: Int): Generator[LocalDateTime] = for {
    year <- IntFromToGenerator(fromYear, toYear)
    month <- IntFromToGenerator(_1, _12)
    day <- IntFromToGenerator(_1, _27)
    hour <- IntFromToGenerator(_0, _23)
    minute <- IntFromToGenerator(_1,_59)
  } yield LocalDateTime.of(year, month, day, hour, minute)

  def ofElem[T](xs: List[T]): Generator[T] = for {
    i <- IntFromToGenerator(0,xs.length - 1)
  } yield xs(i)

  def ofEvent(id: Int): Generator[Event] = for {
    bool <- ofBooleans
    startDate <- ofDates(_2018, _2018)
    title <- ofElem(DataToGenerate.titles)
    description <- ofElem(DataToGenerate.descriptions)
    educator <- ofElem(DataToGenerate.educators)
    salary <- ofElem(DataToGenerate.salaries)
    recipientCategory <- ofElem(models.Recipients.recipients)
    room <- ofElem(DataToGenerate.rooms)
    info <- ofElem(DataToGenerate.infos)
    maxParticipants <- IntFromToGenerator(_1, _20)
  } yield {
    if(bool) {
      EventForIndividual(
        id,
        startDate,
        startDate.plusMinutes(_60),
        title,
        description,
        educator,
        salary,
        recipientCategory,
        room,
        info,
        maxParticipants,
        isCanceled = false,
        Set.empty
      )
    }
    else {
      EventForInstitution(
        id,
        startDate,
        startDate.plusMinutes(_60),
        title,
        description,
        educator,
        salary,
        recipientCategory,
        room,
        info,
        maxParticipants,
        isCanceled = false,
        None,
        None,
        None,
        None
      )
    }
  }

  def  generateEvents(howMany: Int): Seq[Event] = {
    for (i <- 1 to howMany) yield Generators.ofEvent(i).generate
  }
}
