package models

import models.Users._
import models.events.Event
import models.generators.Generators._


object DB extends DataBaseService {
  var users: Set[User] = Set(
    Individual("t", "t", "tomasz", 1234),
    Institution("i", "i", "institutin", 1234, "some name", "street", "school"),
    Admin("a", "a", "Admin"))
  var events: Set[Event] = generateEvents(10).toSet
  var educators: List[Educator] = List(
    Educator("Tomasz Pan", "tomasz@email", 1234),
    Educator("Anna Pani", "anna@email", 1234),
    Educator("Antonina Pani", "antonina@email", 1234),
    Educator("Roza Pani", "roza@email", 1234),
    )

  def listUsers(): Set[User] = users

  def listEvents(): Set[Event] = events

  def listEducators: List[Educator] = educators

  def getUser(email: String): Either[String, User] = users find (_.email == email) match {
    case Some(user) => Right(user)
    case None => Left("There is no such user in database")
  }

  def getEvent(id: Int): Either[String, Event] = events find (_.id == id) match {
    case Some(event) => Right(event)
    case None => Left("There is no such event in datebase")
  }

  def getEducator(email: String): Either[String, Educator] = educators find (_.email == email) match {
    case Some(educator) => Right(educator)
    case None => Left("There is no such teacher in database")
  }

  def save(user: User): Unit = users = users + user

  def save(event: Event): Unit = events = events + event

  def save(educator: Educator): Unit = educators = educators ::: List(educator)

  def update(user: User): Unit = users = users.filter(_.email != user.email) + user

  def update(event: Event): Unit = events = events.filter(_.id != event.id) + event


  def filter(pred: Event => Boolean): Set[Event] = events.filter(pred)
}
