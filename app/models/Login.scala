package models

import play.api.data._
import play.api.data.Forms._

case class Login(email: String, password: String) // 

object Logins {
  def validate(login: Login): Option[String] = {
    val Login(email, password) = login
    DB.getUser(email) match {
      case Right(user) if user.password == password => Some(user.email)
      case _ => None
    }
  }
}
