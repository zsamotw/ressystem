
package models

import models.Users._
import models.events.{Event, EventForInstitution, Events}
import models.forms.{AdditionalDataFromUserForm, IndividualUpdateForm, InstitutionUpdateForm}
import play.api.data.Form

object Helpers {
  def getDataForView(user: User): (Set[Event], List[Educator], List[Recipient]) = {
    (Events.listForUser(user), Educators.listAll, Recipients.recipients)
  }

  def eventCopy(eventData: AdditionalDataFromUserForm, institution: Institution, e: EventForInstitution): EventForInstitution = {
    val newEvent = e.copy(
      howManyParticipants = Some(eventData.howManyParticipants),
      averageAge = Some(eventData.averageAge),
      additionalData = Some(eventData.additionalData),
      subscriber = Some(institution))
    newEvent
  }

  def copyUser(individual: Individual, updateData: IndividualUpdateForm): Individual = {
    val newUser = individual.copy(
      name = updateData.name getOrElse individual.name,
      phone = updateData.phone getOrElse individual.phone)
    newUser
  }

  def copyUser(institution: Institution, updateData: InstitutionUpdateForm): Institution = {
    val newUser = institution.copy(
      repName = updateData.repName getOrElse institution.repName,
      phone = updateData.phone getOrElse institution.phone,
      name = updateData.name getOrElse institution.name,
      address = updateData.address getOrElse institution.address,
      kind = updateData.kind getOrElse institution.kind)
    newUser
  }

  def getFilledForm(newUser: Individual): Form[IndividualUpdateForm] = {
    val dataToFill = IndividualUpdateForm(Some(newUser.name), Some(newUser.phone))
    val filledForm = IndividualUpdateForm.individualUpdateForm.fill(dataToFill)
    filledForm
  }

  def getFilledForm(newUser: Institution): Form[InstitutionUpdateForm] = {
    val dataToFill = InstitutionUpdateForm(
      Some(newUser.repName),
      Some(newUser.phone),
      Some(newUser.name),
      Some(newUser.address),
      Some(newUser.kind))
    val filledForm = InstitutionUpdateForm.institutionUpdateForm.fill(dataToFill)
    filledForm
  }
}
