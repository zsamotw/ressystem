
package models.reports

import models._
import models.events._
import scala.math.BigDecimal

trait Report {
  val events: Set[Event]
  val eventsForIndividuals: Set[EventForIndividual] = events
    .filter(EventFilters.kindFilter(Some(Events.forIndividualFlag)))
    .asInstanceOf[Set[EventForIndividual]]
  val eventsForInstitutions: Set[EventForInstitution] = events
    .filter(EventFilters.kindFilter(Some(Events.forInstitutionFlag)))
    .asInstanceOf[Set[EventForInstitution]]

  def countEvents: Int = eventsForIndividuals.size + eventsForInstitutions.size

  def eventsBySubscribers: List[EventForIndividual] = eventsForIndividuals
    .toList
    .sortBy(_.subscribers.size)
    .reverse

  def computeSalary: Map[Educator, BigDecimal] = events
    .groupBy(_.educator)
    .mapValues(_.foldLeft(BigDecimal(0))(_ + _.salary))
}
