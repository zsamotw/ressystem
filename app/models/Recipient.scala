package models

case class Recipient(val code: String, val description: String)

object Recipients {

  val from3to5 = "from3to5"
  val from5to7 = "from5to7"
  val primaryschool = "primaryschool"
  val highschool = "highschool"

  val recipients = List(Recipient(from3to5, "For Children 3 to 5"), Recipient(from5to7, "For Children 5 to 7"), Recipient(primaryschool, "For Primary School"), Recipient(highschool, "For High School"))

  def find(code: String): Either[String, Recipient] = {
    val result = recipients.find(_.code == code)
    result match {
      case Some(recipient) => Right(recipient)
      case None => Left("No such category of recipient")
    }
  }
}
