package models.forms

import play.api.data._
import play.api.data.Forms._

case class AdditionalDataFromUserForm(
  howManyParticipants: Int,
  averageAge:          Int,
  additionalData:      String)

object AdditionalDataFromUserForm {
  val additionalDataFromUserForm = Form(
    mapping(
      "howManyParticipants" -> number,
      "averageAge" -> number,
      "additionalData" -> text)(AdditionalDataFromUserForm.apply)(AdditionalDataFromUserForm.unapply))
}
