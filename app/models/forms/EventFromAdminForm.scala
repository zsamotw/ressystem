
package models.forms

import play.api.data.Forms._
import play.api.data._

case class EventFromAdminForm(
                               startDate: java.util.Date,
                               startTime: String,
                               endDate: java.util.Date,
                               endTime: String,
                               title: String,
                               description: String,
                               whatKind: String,
                               educatorEmail: String,
                               salary: Int,
                               recipientCategory: String,
                               room: String,
                               info: String,
                               maxParticipants: Int)

object EventFromAdminForm {
  val eventAdminForm = Form(
    mapping(
      "startDate" -> date,
      "startTime" -> default(text, "00:00"),
      "endDate" -> date,
      "endTime" -> default(text, "00:00"),
      "title" -> text,
      "description" -> text,
      "kind" -> text,
      "educator" -> text,
      "salary" -> number,
      "recipientCategory" -> text,
      "room" -> text,
      "info" -> text,
      "max" -> number)(EventFromAdminForm.apply)(EventFromAdminForm.unapply))
}
