
package models.forms

import java.util.Date

import play.api.data.Forms._
import play.api.data._

case class DataToGenerateReport(
                                 from: Option[Date],
                                 to: Option[Date])


object ReportGenerateForm {
  val dataToGenerateReport = Form(
    mapping(
      "from" -> optional(date),
      "to" -> optional(date))(DataToGenerateReport.apply)(DataToGenerateReport.unapply))
}
