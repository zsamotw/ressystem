
package models.forms

import play.api.data.Forms._
import play.api.data._

case class IdForm(id: Int)

object IdForm {
  val idForm = Form(mapping("id" -> number)(IdForm.apply)(IdForm.unapply))
}
