
package models.forms

import models.Login
import play.api.data.Forms._
import play.api.data._

object LoginForm {
  val loginForm = Form(
    mapping(
      "email" -> text,
      "password" -> text,
    )(Login.apply)(Login.unapply))
}
