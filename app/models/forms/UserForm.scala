
package models.forms

import play.api.data.Forms._
import play.api.data._

case class UserForm(
                     email: String,
                     password: String,
                     name: String,
                     phone: Int,
                     institutionName: Option[String],
                     institutionAddress: Option[String],
                     institutionKind: Option[String])

object UserForm {
  val userForm = Form(
    mapping(
      "email" -> text,
      "password" -> text,
      "name" -> text,
      "phone" -> number,
      "instName" -> optional(text),
      "instAddress" -> optional(text),
      "instKind" -> optional(text)
    )(UserForm.apply)(UserForm.unapply))
}
