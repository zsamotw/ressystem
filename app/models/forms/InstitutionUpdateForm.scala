
package models.forms

import play.api.data.Forms._
import play.api.data._

case class InstitutionUpdateForm(
                                  repName: Option[String], phone: Option[Int],
                                  name: Option[String],
                                  address: Option[String],
                                  kind: Option[String])

object InstitutionUpdateForm {
  val institutionUpdateForm = Form(
    mapping("repName" -> optional(text),
      "phone" -> optional(number),
      "name" -> optional(text),
      "address" -> optional(text),
      "kind" -> optional(text)
    )(InstitutionUpdateForm.apply)(InstitutionUpdateForm.unapply))
}
