
package models.forms

import play.api.data.Forms._
import play.api.data._

case class IndividualUpdateForm(
                                 name: Option[String],
                                 phone: Option[Int])

object IndividualUpdateForm {
  val individualUpdateForm = Form(
    mapping(
      "name" -> optional(text),
      "phone" -> optional(number)
    )(IndividualUpdateForm.apply)(IndividualUpdateForm.unapply))
}
