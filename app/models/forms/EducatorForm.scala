package models.forms

import models.Educator
import play.api.data._
import play.api.data.Forms._

object EducatorForm {
  val educatorForm = Form(
    mapping(
      "name" -> text,
      "email" -> text,
      "phone" -> number,
      )(Educator.apply)(Educator.unapply)
  )
}
