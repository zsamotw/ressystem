package models.forms

import java.util.Date
import play.api.data._
import play.api.data.Forms._

case class DataToFilterEventsByUserForm(
  from: Option[Date],
  to: Option[Date],
  keyword: Option[String],
  educator: Option[String],
  recipientCategory: Option[String])

object DataToFilterEventsByUserForm {
  val dataToFilterEventsByUserForm = Form(
    mapping(
      "from" -> optional(date),
      "to" -> optional(date),
      "keyword" -> optional(text),
      "educator" -> optional(text),
      "recipient" -> optional(text))(DataToFilterEventsByUserForm.apply)(DataToFilterEventsByUserForm.unapply))
}
