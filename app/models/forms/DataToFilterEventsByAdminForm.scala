
package models.forms

import java.util.Date
import play.api.data._
import play.api.data.Forms._

case class DataToFilterEventsByAdminForm(
  from: Option[Date],
  to: Option[Date],
  keyword: Option[String],
  educator: Option[String],
  whatKind: Option[String],
  status: Option[String],
  recipientCategory: Option[String])

object DataToFilterEventsByAdminForm {
  val dataToFilterEventsByAdminForm = Form(
    mapping(
      "from" -> optional(date),
      "to" -> optional(date),
      "keyword" -> optional(text),
      "educator" -> optional(text),
      "kind" -> optional(text),
      "status" -> optional(text),
      "recipient" -> optional(text))(DataToFilterEventsByAdminForm.apply)(DataToFilterEventsByAdminForm.unapply))
}

