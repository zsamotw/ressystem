package models

trait Status

object Active extends Status
object Realized extends Status
object Canceled extends Status

object Statuses {
  val active = "activ"
  val realized = "realized"
  val canceled = "canceled"
  val statuses= Map(active -> "Active", realized -> "Realized", canceled -> "Canceled")

  def applay(flag: String): Status = flag match {
    case `active` => Active
    case `realized` => Realized
    case `canceled` => Canceled
  }

  def get(statusFlag: String): Option[Status] = {
    if(statusFlag == active) Some(Active)
    else if(statusFlag == realized) Some(Realized)
    else if(statusFlag == canceled) Some(Canceled)
    else None
  }
}
