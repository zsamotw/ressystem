package models

trait InstitutionType

object Kindergarten extends InstitutionType
object PrimarySchool extends InstitutionType
