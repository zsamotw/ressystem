package models

case class Educator(name: String, email: String, phone: Int)

object Educators {
  val db: DataBaseService = DB

  def listAll: List[Educator] = db.listEducators

  def save(educator: Educator): Unit = db.save(educator)

  def find(email: String): Either[String, Educator] = {
    db.getEducator(email)
  }
}
