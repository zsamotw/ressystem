
package models

import models.events.Event
import models.Users._

abstract class DataBaseService {
  def listUsers(): Set[User]

  def listEvents(): Set[Event]

  def listEducators: List[Educator]

  def getUser(email: String): Either[String, User]

  def getEvent(id: Int): Either[String, Event]

  def getEducator(email: String): Either[String, Educator]

  def save(user: User): Unit

  def save(event: Event): Unit

  def save(teacher: Educator): Unit

  def update(event: Event): Unit

  def update(user: User): Unit

  def filter(pred: Event => Boolean): Set[Event]
}
