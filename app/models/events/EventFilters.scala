
package models.events

import java.time._

import models._
import models.Users._

object EventFilters {
  type EventFilter = Event => Boolean

  def everyFilters(filters: EventFilter*): EventFilter = event => filters forall (filter => filter(event))

  val eventsSubscribedForUser: User => EventFilter = user => event => (user, event) match {
    case (i: Individual, e: EventForIndividual) => e.subscribers contains i
    case (i: Institution, e: EventForInstitution) => e.subscriber match {
      case None => false
      case Some(institution) => i == institution
    }
    case (_, _) => false
  }

  val eventsForUserType: User => EventFilter = user => event => (user, event) match {
    case (i: Individual, e: EventForIndividual) => true
    case (i: Institution, e: EventForInstitution) => true
    case (a: Admin, _) => true
    case (_, _) => false
  }

  val toDateFilter: Option[LocalDateTime] => EventFilter = dateOpt => event => dateOpt match {
    case None => true
    case Some(date) => if(event.startDate.isBefore(date.plusDays(1))) true else false
  }

  val fromDateFilter: Option[LocalDateTime] => EventFilter = dateOpt => event => dateOpt match {
    case None => true
    case Some(date) => if(event.startDate.isAfter(date.minusDays(1))) true else false
  }

  val stringFilter: Option[String] => EventFilter = strOpt => event => strOpt match {
    case None => true
    case Some(s) => if((event.title contains s) || (event.description contains s)) true else false
  }

  val educatorFilter: Option[String] => EventFilter = emailOpt => event => emailOpt match {
    case None => true
    case Some(email) => if(event.educator.email == email) true else false
  }

  val recipientFilter: Option[String] => EventFilter = recipientCodeOpt => event => recipientCodeOpt match {
    case None => true
    case Some(recipientCode) => if(event.recipientCategory.code == recipientCode) true else false
  }

  val kindFilter: Option[String] => EventFilter = kindFlagOpt => event =>  kindFlagOpt match {
    case None => true
    case Some(kindFlag) =>
      (kindFlag, event) match {
        case (Events.forIndividualFlag, EventForIndividual(_,_,_,_,_,_,_,_,_,_,_,_,_)) => true
        case (Events.forInstitutionFlag, EventForInstitution(_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_)) => true
        case (_, _) => false
      }
  }

  val statusFilter: Option[String] => EventFilter = statusFlagOpt => event => statusFlagOpt match {
    case None => true
    case Some(statusFlag) =>
      Statuses.get(statusFlag) match {
        case None => true
        case Some(status) =>
          if(status == event.status) true else false
      }
  }

  /*
   * filters todo: isCanceled, isEnoughtPlaces, ordering
   */
}
