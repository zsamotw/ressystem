
package models.events

import java.time._
import java.time.format.DateTimeFormatter

import models.{Educator, Recipient, Status}

trait Event {
  val id: Int
  val startDate: LocalDateTime
  val endDate: LocalDateTime
  val title: String
  val description: String
  val educator: Educator
  val salary: BigDecimal
  val recipientCategory: Recipient
  val room: String
  val info: String
  val maxParticipants: Int
  val isCanceled: Boolean
  val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")

  def isEnoughPlaces: Boolean

  def freePlaces: Int

  def status: Status

  def dateFormat(date: LocalDateTime): String = dateFormatter.format(date)
}
