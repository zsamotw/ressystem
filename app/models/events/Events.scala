
package models.events

import java.time._
import java.util.Date

import models.Users.User
import models._
import models.forms.{DataToFilterEventsByAdminForm, DataToFilterEventsByUserForm, EventFromAdminForm}

object Events {

  lazy val forInstitutionFlag = "forinstitution"
  lazy val forIndividualFlag = "forindividual"
  lazy val kinds = Map(forIndividualFlag -> "For individuals", forInstitutionFlag -> "for institutions")

  val db: DataBaseService = DB

  def listAll: Set[Event] = db.listEvents()

  def save(event: Event): Unit = db.save(event)

  def find(eventId: Int): Either[String, Event] = db.getEvent(eventId)

  def no: Int = db.listEvents().size

  def update(event: Event): Unit = db.update(event)

  def filter(pred: Event => Boolean): Set[Event] = db.filter(pred)

  def listForUser(user: User): Set[Event] = {
    filter(EventFilters.eventsForUserType(user))
  }

  def listSubscribedEvents(user: User): Set[Event] = {
    filter(EventFilters.eventsSubscribedForUser(user))
  }

  def form2Event(eventData: EventFromAdminForm): Either[String, Event] = {
    val EventFromAdminForm(startDate, startTime, endDate, endTime, title, description, whatType, educatorEmail, salary, recipientCategoryCode, room, info, maxParticipants) = eventData
    getTime(startTime) match {
      case Right(startPoint) =>
        getTime(endTime) match {
          case Right(endPoint) =>
            val startDateLD = Events.date2LocalDate(startDate, startPoint)
            val endDateLD = Events.date2LocalDate(endDate, endPoint)
            Educators.find(educatorEmail) match {
              case Right(educator) =>
                val no = Events.no + 1
                Recipients.find(recipientCategoryCode) match {
                  case Right(recipientsCategory) =>
                    whatType match {
                      case `forInstitutionFlag` =>
                        Right(EventForInstitution(no, startDateLD, endDateLD, title, description, educator, salary, recipientsCategory, room, info, maxParticipants, isCanceled = false, None, None, None, None))
                      case `forIndividualFlag` =>
                        Right(EventForIndividual(no, startDateLD, endDateLD, title, description, educator, salary, recipientsCategory, room, info, maxParticipants, isCanceled = false, Set()))
                      case _ => Left("No such category")
                    }
                  case Left(message) => Left(message)
                }
              case Left(message) => Left(message)
            }
          case Left(message) => Left(message)
        }
      case Left(message) => Left(message)
    }
  }

  def date2LocalDate(date: Date, point: (Int, Int)): LocalDateTime = {
    val (hour, minute) = point
    val instant = date.toInstant
    LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).withHour(hour).withMinute(minute)
  }

  def dateOpt2LocalDateOpt(date: Option[Date]): Option[LocalDateTime] = date match {
    case None => None
    case Some(d) =>
      val instant = d.toInstant
      val localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
      Some(localDate)
  }

  def getTime(str: String): Either[String, (Int, Int)] = {
    val pattern = """(\d{2}):(\d{2})""".r
    str match {
      case pattern(hour, minutes) =>
        Right((hour.toInt, minutes.toInt))
      case _ => Left("Wrong pattern of hour. Use hh:mm.")
    }
  }

  def getDataWithFilteredEvents(dataToFilter: DataToFilterEventsByUserForm, user: User): (Set[Event], List[Educator], List[Recipient], String) = {
    val DataToFilterEventsByUserForm(from, to, keyword, educatorEmail, recipientCode) = dataToFilter
    val toLocalDate = Events.dateOpt2LocalDateOpt(to)
    val fromLocalDate = Events.dateOpt2LocalDateOpt(from)
    val events = Events.filter(
      EventFilters.everyFilters(
        EventFilters.eventsForUserType(user),
        EventFilters.toDateFilter(toLocalDate),
        EventFilters.fromDateFilter(fromLocalDate),
        EventFilters.stringFilter(keyword),
        EventFilters.educatorFilter(educatorEmail),
        EventFilters.recipientFilter(recipientCode)
      )
    )
    val educators = Educators.listAll
    val recipients = Recipients.recipients
    val message = "Filtered events here:"
    (events, educators, recipients, message)
  }

  def getDataWithFilteredEvents(dataToFilter: DataToFilterEventsByAdminForm, user: User): (Set[Event], List[Educator], List[Recipient], String) = {
    val DataToFilterEventsByAdminForm(from, to, keyword, educatorEmail, whatKind, status, recipientCode) = dataToFilter
    val toLocalDate = Events.dateOpt2LocalDateOpt(to)
    val fromLocalDate = Events.dateOpt2LocalDateOpt(from)
    val events = Events.filter(
      EventFilters.everyFilters(
        EventFilters.eventsForUserType(user),
        EventFilters.toDateFilter(toLocalDate),
        EventFilters.fromDateFilter(fromLocalDate),
        EventFilters.stringFilter(keyword),
        EventFilters.educatorFilter(educatorEmail),
        EventFilters.kindFilter(whatKind),
        EventFilters.statusFilter(status),
        EventFilters.recipientFilter(recipientCode)
      )
    )
    val educators = Educators.listAll
    val recipients = Recipients.recipients
    val message = "Filtered events here:"
    (events, educators, recipients, message)
  }

}
