
package models.events

import java.time._

import models.Users.Institution
import models._

case class EventForInstitution(
  id: Int,
  startDate: LocalDateTime,
  endDate: LocalDateTime,
  title: String,
  description: String,
  educator: Educator,
  salary: BigDecimal,
  recipientCategory: Recipient,
  room: String,
  info: String,
  maxParticipants: Int,
  isCanceled: Boolean,
  howManyParticipants: Option[Int],
  averageAge: Option[Int],
  additionalData: Option[String],
  subscriber: Option[Institution]) extends Event {

  def isEnoughPlaces: Boolean = subscriber.isEmpty

  def freePlaces: Int = if(subscriber isEmpty) 1 else 0

  def status: Status = {
    if(isCanceled) Canceled
    else {
      if(LocalDateTime.now().isBefore(startDate)) Active
      else {
        if(subscriber isEmpty) Canceled
        else Realized
      }
    }
  }

  def subscriberToString = subscriber match {
    case None => "No one"
    case Some(instituton) => instituton.name
  }
}
