
package models.events

import java.time._

import models.Users.Individual
import models._

case class EventForIndividual(
  id: Int,
  startDate: LocalDateTime,
  endDate: LocalDateTime,
  title: String,
  description: String,
  educator: Educator,
  salary: BigDecimal,
  recipientCategory: Recipient,
  room: String,
  info: String,
  maxParticipants: Int,
  isCanceled: Boolean,
  subscribers: Set[Individual]) extends Event {

  def isEnoughPlaces: Boolean = maxParticipants > subscribers.size

  def freePlaces: Int = maxParticipants - subscribers.size

  def status: Status = {
    if(isCanceled) Canceled
    else {
      if(LocalDateTime.now().isBefore(startDate)) Active
      else {
        if(subscribers isEmpty) Canceled
        else Realized
      }
    }
  }
}
