
package models

object Users {

  trait User {
    val email: String
    val password: String
    val name: String
  }

  case class Admin(
    email: String,
    password: String,
    name: String,
    ) extends User

  case class Individual(
    email: String,
    password: String,
    name: String,
    phone: Int
  ) extends User

  case class Institution(
    email: String,
    password: String,
    repName: String,
    phone: Int,
    name: String,
    address: String,
    kind: String
  ) extends User

  val db: DataBaseService = DB

  def listAll: Set[User] = db.listUsers()

  def save(user: User): Unit = db.save(user)

  def find(email: String): Either[String, User] = {
    db.getUser(email)
  }

  def update(user: User): Unit = db.update(user)

}
